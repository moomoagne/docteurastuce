package com.docteur.astuce.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.docteur.astuce.entity.Roles;


public interface RoleRepository  extends JpaRepository<Roles,Integer>{

}
