package com.docteur.astuce.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.docteur.astuce.entity.UserMedecin;

public interface UserMedecinRepository extends JpaRepository<UserMedecin, Integer>{

}
