package com.docteur.astuce.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.docteur.astuce.entity.Conseil;


public interface ConseilRepository  extends JpaRepository<Conseil,Integer>{

}
