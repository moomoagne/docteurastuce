package com.docteur.astuce;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.filter.CharacterEncodingFilter;


@SpringBootApplication
public class ApiDocteurAstuceApplication extends SpringBootServletInitializer implements CommandLineRunner{
    
  
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ApiDocteurAstuceApplication.class);
    }
    public static void main(String[] args) throws Exception {
        SpringApplication.run(ApiDocteurAstuceApplication.class, args);
    }
    
    @Override
    public void run(String... arg0) throws Exception {
        /*fileStorageService.deleteAll();
        fileStorageService.init();*/
    }
    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    CharacterEncodingFilter characterEncodingFilter() {
      CharacterEncodingFilter filter = new CharacterEncodingFilter();
      filter.setEncoding("UTF-8");
      filter.setForceEncoding(true);
      return filter;
    }
    
   
}