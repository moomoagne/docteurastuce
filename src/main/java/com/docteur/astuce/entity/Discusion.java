/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.docteur.astuce.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author macbookpro
 */
@Entity
@Table(name = "DISCUSION")
@XmlRootElement
public class Discusion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_discusion")
    private Integer idDiscusion;
    @Size(max = 254)
    @Column(name = "contenu")
    private String contenu;
    @Column(name = "date_discusion")
    @Temporal(TemporalType.DATE)
    private Date dateDiscusion;
    
    @JoinColumn(name = "user_medecin_id_user_medecin", referencedColumnName = "id_user")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User userMedecinIdUserMedecin;
    
    @JoinColumn(name = "user_patient_id_user_patient", referencedColumnName = "id_user")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User userPatientIdUserPatient;
    
    public User getUserMedecinIdUserMedecin() {
		return userMedecinIdUserMedecin;
	}

	public void setUserMedecinIdUserMedecin(User userMedecinIdUserMedecin) {
		this.userMedecinIdUserMedecin = userMedecinIdUserMedecin;
	}

	public User getUserPatientIdUserPatient() {
		return userPatientIdUserPatient;
	}

	public void setUserPatientIdUserPatient(User userPatientIdUserPatient) {
		this.userPatientIdUserPatient = userPatientIdUserPatient;
	}

	public Discusion() {
    }

    public Discusion(Integer idDiscusion) {
        this.idDiscusion = idDiscusion;
    }

    public Integer getIdDiscusion() {
        return idDiscusion;
    }

    public void setIdDiscusion(Integer idDiscusion) {
        this.idDiscusion = idDiscusion;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public Date getDateDiscusion() {
        return dateDiscusion;
    }

    public void setDateDiscusion(Date dateDiscusion) {
        this.dateDiscusion = dateDiscusion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDiscusion != null ? idDiscusion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Discusion)) {
            return false;
        }
        Discusion other = (Discusion) object;
        if ((this.idDiscusion == null && other.idDiscusion != null) || (this.idDiscusion != null && !this.idDiscusion.equals(other.idDiscusion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.docteurastuce.model.Discusion[ idDiscusion=" + idDiscusion + " ]";
    }
    
}
