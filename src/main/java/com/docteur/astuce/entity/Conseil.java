/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.docteur.astuce.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Proxy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 *
 * @author macbookpro
 */
@Entity
@Table(name = "CONSEIL")
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = false)
@Proxy(lazy = false)

public class Conseil implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_conseil",unique=true, nullable = false)
    private Integer idConseil;
    
    @Size(max = 254)
    @Column(name = "titre")
    private String titre;
    @Size(max = 254)
    @Column(name = "contenu")
    private String contenu;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_contenu")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateContenu;
    
    @JoinColumn(name = "user_medecin_id_user_medecin", referencedColumnName = "id_user")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private UserMedecin userMedecinIdUserMedecin;
    
   


	public UserMedecin getUserMedecinIdUserMedecin() {
		return userMedecinIdUserMedecin;
	}

	public void setUserMedecinIdUserMedecin(UserMedecin userMedecinIdUserMedecin) {
		this.userMedecinIdUserMedecin = userMedecinIdUserMedecin;
	}

	public Conseil() {
    }

    public Conseil(Integer idConseil) {
        this.idConseil = idConseil;
    }

    public Conseil(Integer idConseil, Date dateContenu) {
        this.idConseil = idConseil;
        this.dateContenu = dateContenu;
    }

    public Integer getIdConseil() {
        return idConseil;
    }

    public void setIdConseil(Integer idConseil) {
        this.idConseil = idConseil;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public Date getDateContenu() {
        return dateContenu;
    }

    public void setDateContenu(Date dateContenu) {
        this.dateContenu = dateContenu;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idConseil != null ? idConseil.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Conseil)) {
            return false;
        }
        Conseil other = (Conseil) object;
        if ((this.idConseil == null && other.idConseil != null) || (this.idConseil != null && !this.idConseil.equals(other.idConseil))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.docteurastuce.model.Conseil[ idConseil=" + idConseil + " ]";
    }
   
}
