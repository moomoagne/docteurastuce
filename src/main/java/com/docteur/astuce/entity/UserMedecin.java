/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.docteur.astuce.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Proxy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 * @author macbookpro
 */
@Entity
@Table(name = "USER_MEDECIN")
@DiscriminatorValue("UM")
@JsonIgnoreProperties(ignoreUnknown = false)
@Proxy(lazy = false)
@XmlRootElement
public class UserMedecin extends User {
    private static final long serialVersionUID = 1L;
   
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userMedecinIdUserMedecin", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Conseil> conseilList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userMedecinIdUserMedecin", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Discusion> discussionlList;
    
    public List<Discusion> getDiscussionlList() {
		return discussionlList;
	}

	public void setDiscussionlList(List<Discusion> discussionlList) {
		this.discussionlList = discussionlList;
	}

	public List<Conseil> getConseilList() {
		return conseilList;
	}

	public void setConseilList(List<Conseil> conseilList) {
		this.conseilList = conseilList;
	}

	public UserMedecin() {
    }

	public UserMedecin(String nom, String prenom, String adresse, Integer age, String email, Integer telephone,
			Date datenaiss, String sexe, Date dateInscription, Roles role) {
		super(nom, prenom, adresse, age, email, telephone, datenaiss, sexe, dateInscription, role);
		// TODO Auto-generated constructor stub
	}

  
    

}
