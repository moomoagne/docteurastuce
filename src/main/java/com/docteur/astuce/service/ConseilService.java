package com.docteur.astuce.service;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

import com.docteur.astuce.dao.ConseilRepository;
import com.docteur.astuce.entity.Conseil;

@SpringBootApplication
@ComponentScan(basePackages = {"com.docteur.astuce.rest","com.docteur.astuce.service"})
@EntityScan("com.docteur.astuce.entity")
public class ConseilService {

	@Autowired
	ConseilRepository conseilRepository;
	
	public List<Conseil> getAllConseil(){
		List<Conseil> conseils = new ArrayList<>();
		conseilRepository.findAll().forEach(conseils::add);
		return conseils;
	}
	
	public Conseil getConseil(Integer id) {
		return conseilRepository.findOne(id);
	}
	
	public void addConseil(Conseil conseil) {	
		conseilRepository.save(conseil);
	}

	public void updateConseil(Integer id, Conseil conseil) {
		conseilRepository.saveAndFlush(conseil);
	}

	public void deleteConseil(Integer id) {
		conseilRepository.delete(id);
	}
	
}
