package com.docteur.astuce.rest;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.docteur.astuce.dao.ConseilRepository;
import com.docteur.astuce.dao.UserMedecinRepository;
import com.docteur.astuce.dao.UserRepository;
import com.docteur.astuce.entity.Conseil;
import com.docteur.astuce.entity.UserMedecin;
import com.docteur.astuce.service.ConseilService;


@RequestMapping("/docteur_astuce_project")
@RestController
public class ConseilControllerRest {

	@Autowired
	ConseilService conseilService;
	@Autowired
	ConseilRepository conseilRepository;
	@Autowired
	UserRepository userRepository;
	@Autowired
	UserMedecinRepository userMedecinRepository;


	@RequestMapping(value = "/conseils", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public Map<String,Object> getAllConseil(){
		HashMap<String, Object> h = new HashMap<String, Object>();
		List<Conseil> conseilList = conseilRepository.findAll();
		System.out.println("Liste consels -----"+conseilList.size());
		h.put("message", "liste des conseils");
		h.put("status", 0);
		h.put("conseil_list", conseilList);
		return h;

	}

	@RequestMapping(value = "/conseils/{id}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public  Map<String,Object> getConseil(@PathVariable Integer id) {
		HashMap<String, Object> h = new HashMap<String, Object>();
		Conseil conseil ;
		if(id == null )
		{
			h.put("status", -1);
			h.put("message", "1 ou plusieurs paramètres manquants");
			return h;
		}

		try {

			conseil = conseilService.getConseil(id);
			if(conseil == null)
			{
				h.put("message", "Le conseil  n'existe pas.");
				h.put("status", -1);
				return h;
			}

		} catch (Exception e) {
			e.printStackTrace();
			h.put("message", "Oups, une erreur a été détectée \""+e.getMessage()+"\" ... veuillez réessayer ou contacter le support.");
			h.put("status", -1);
			return h;
		}
		h.put("message", "Conseil trouvé");
		h.put("status", 0);
		h.put("conseil", conseil);
		return h;

	}

	@RequestMapping(value="/conseils/{id}",method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public Map<String,Object> addConseil(@RequestBody Conseil conseil,@PathVariable Integer id){
		HashMap<String, Object> h = new HashMap<String, Object>();

		if(conseil == null || id == null )
		{
			h.put("status", -1);
			h.put("message", "1 ou plusieurs paramètres manquants");
			return h;
		}
		try {
			UserMedecin user = userMedecinRepository.findOne(id);
			if(user == null){
				h.put("message", "L'user n'existe pas.");
				h.put("status", -1);
				return h;
			}
			if(!user.getRoleIdRole().getNomRole().equals("MEDECIN")){
				h.put("message", "Vous n'avez pas le profil pour effectuer cette tache.");
				h.put("status", -1);
				return h;
			}
			System.out.println("111111111111");
			conseil.setUserMedecinIdUserMedecin(user);
			conseil.setDateContenu(new Date());
			System.out.println("Conseil user "+conseil.getUserMedecinIdUserMedecin().getIdUser());
			System.out.println("Conseil titre "+conseil.getTitre());
			conseilRepository.save(conseil);
		} catch (Exception e) {
			e.printStackTrace();
			h.put("message", "Oups, une erreur a été détectée \""+e.getMessage()+"\" ... veuillez réessayer ou contacter le support.");
			h.put("status", -1);
			return h;
		}
		h.put("message", "Conseil ajouté avec succes ");
		h.put("status", 0);
		h.put("conseil", conseil);
		return h;
	}

	@RequestMapping( value="/conseils/{id}",method=RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public Map<String,Object> updateConseil(@RequestBody Conseil conseil, @PathVariable Integer id){
		HashMap<String, Object> h = new HashMap<String, Object>();

		if(conseil == null || id == null )
		{
			h.put("status", -1);
			h.put("message", "1 ou plusieurs paramètres manquants");
			return h;
		}
		try {
			UserMedecin user = userMedecinRepository.findOne(id);
			if(user == null){
				h.put("message", "L'user n'existe pas.");
				h.put("status", -1);
				return h;
			}
			if(!user.getRoleIdRole().getNomRole().equals("MEDECIN")){
				h.put("message", "Vous n'avez pas le profil pour effectuer cette tache.");
				h.put("status", -1);
				return h;
			}
			conseilService.updateConseil(id, conseil);
		} catch (Exception e) {
			e.printStackTrace();
			h.put("message", "Oups, une erreur a été détectée \""+e.getMessage()+"\" ... veuillez réessayer ou contacter le support.");
			h.put("status", -1);
			return h;
		}
		h.put("message", "Conseil modifié avec succes ");
		h.put("status", 0);
		h.put("conseil", conseil);
		return h;
	}

	@RequestMapping(value="/conseils/{id}/{idUser}",method=RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public  Map<String,Object>  deleteConseil(@PathVariable Integer id,@PathVariable Integer idUser) {
		HashMap<String, Object> h = new HashMap<String, Object>();

		//Fields Control
		if(id == null || idUser == null)
		{
			h.put("status", -1);
			h.put("message", "1 ou plusieurs paramètres manquants");
			return h;
		}
		try {
			UserMedecin user = userMedecinRepository.findOne(idUser);
			if(user == null){
				h.put("message", "L'user n'existe pas.");
				h.put("status", -1);
				return h;
			}
			if(!user.getRoleIdRole().getNomRole().equals("MEDECIN")){
				h.put("message", "Vous n'avez pas le profil pour effectuer cette tache.");
				h.put("status", -1);
				return h;
			}
			Conseil conseilFound = conseilService.getConseil(id);
			if(conseilFound == null){
				h.put("message", "Le conseil n'existe pas.");
				h.put("status", -1);
				return h;
			}
			conseilService.deleteConseil(id);
		} catch (Exception e) {
			e.printStackTrace();
			h.put("message", "Oups, une erreur a été détectée \""+e.getMessage()+"\" ... veuillez réessayer ou contacter le support.");
			h.put("status", -1);
			return h;
		}
		h.put("message", "Le conseil a été bien supprimé.");
		h.put("status", 0);
		return h;
	}

}
