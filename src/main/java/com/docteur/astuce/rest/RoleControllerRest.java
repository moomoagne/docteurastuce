package com.docteur.astuce.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.docteur.astuce.dao.RoleRepository;
import com.docteur.astuce.entity.Roles;
@RequestMapping("/docteur_astuce_project")
@RestController
public class RoleControllerRest {

	@Autowired
	RoleRepository roleRepository;
	
	@RequestMapping(value = "/role/list", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public Map<String,Object> listUser() {
		HashMap<String, Object> h = new HashMap<String, Object>();
		
		List<Roles> roles = roleRepository.findAll();
		System.out.println("List size ----- "+roles.size());
			h.put("status", 0);
			h.put("role_list", roles);
			h.put("message", "list OK");
		
		
		return h;
	}
}
